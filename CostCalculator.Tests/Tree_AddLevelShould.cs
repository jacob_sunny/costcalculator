using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CostCalculator.Tests
{
    [TestClass]
    public class Tree_AddLevelShould
    {
        private readonly  Tree _tree;

        public Tree_AddLevelShould()
        {
            _tree = new Tree();
        }

        [TestMethod]
        public void InitaliseCostsForFirstLevel()
        {
            _tree.AddLevel(new double[] { 1 });

            Assert.AreEqual(1, _tree.GetMaxCost());
        }

        [TestMethod]
        public void HandleNegativeCosts()
        {
            _tree.AddLevel(new double[] { -1 });

            Assert.AreEqual(-1, _tree.GetMaxCost());
        }

        [TestMethod]
        public void CalculateCosts()
        {
            _tree.AddLevel(new double[] { 1 });
            _tree.AddLevel(new double[] { 2, 3 });

            Assert.AreEqual(4, _tree.GetMaxCost());
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void ThrowExceptionOnInvalidRowLength()
        {
            _tree.AddLevel(new double[] { 1 });
            _tree.AddLevel(new double[] { 2, 3, 4 });

            Assert.AreEqual(4, _tree.GetMaxCost());
        }
    }
}