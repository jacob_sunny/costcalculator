using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CostCalculator.Tests
{
    [TestClass]
    public class Program_MainShould
    {
        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void ThrowExceptionOnMissingFileArgument()
        {
            Program.Main(new string[] { });
        }

        [TestMethod]
        public void CalculateCosts()
        {
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Console.SetOut(sw);
            Program.Main(new string[] { @"Data/Data1.txt" });
            Assert.AreEqual("Maximum Cost from root to leaf is 114\n", sw.ToString());
        }

        [TestMethod]
        public void CalculateCostsForSingleLevelTree()
        {
            System.IO.StringWriter sw = new System.IO.StringWriter();
            System.Console.SetOut(sw);
            Program.Main(new string[] { @"Data/Data2.txt" });
            Assert.AreEqual("Maximum Cost from root to leaf is 1\n", sw.ToString());
        }
    }
}