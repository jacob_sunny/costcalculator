using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace CostCalculator.Tests
{
    [TestClass]
    public class FileReader_ProcessTreeFromFileShould
    {
        [TestMethod]
        public void IgnoreMultipleSpaces()
        {
            int numberCount = 1;
            foreach ( var line in FileReader.ProcessTreeFromFile(@"Data/Data3.txt"))
            {
                Assert.AreEqual(numberCount++, line.Length);
                foreach (var number in line)
                {
                    Assert.AreEqual(-1.5, number);
                }
            }
        }

        [TestMethod]
        [ExpectedException(typeof(System.ArgumentException))]
        public void ThrowExceptionOnInvalidInput()
        {
            int numberCount = 1;
            foreach (var line in FileReader.ProcessTreeFromFile(@"Data/Data4.txt"))
            {
                Assert.AreEqual(numberCount++, line.Length);
            }
        }
    }
}