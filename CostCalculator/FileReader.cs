﻿namespace CostCalculator
{
    public static class FileReader
    {
        // Opens a file with the given filename and returns an iterator of the parsed lines
        // Any errors thrown in file stream handling will crash the program
        public static System.Collections.Generic.IEnumerable<double[]> ProcessTreeFromFile(string fileName)
        {
            string line;
            System.IO.StreamReader file = null;
            file = new System.IO.StreamReader(fileName);
            while ((line = file.ReadLine()) != null)
            {
                yield return ParseArrayFromString(line);
            }
            if (file != null)
            {
                file.Close();
            }

        }

        // Parses a line of space-separated group of numbers into a double[]
        private static double[] ParseArrayFromString(string line)
        {
            string[] level = line.Split(new char[0], System.StringSplitOptions.RemoveEmptyEntries);
            double[] numbers = new double[level.Length];

            int index = 0;

            foreach (string value in level)
            {
                if (double.TryParse(value, out double number))
                    numbers[index++] = number;
                else
                    throw new System.ArgumentException($"Unable to parse '{value}' in file");
            }
            return numbers;
        }
    }
}
