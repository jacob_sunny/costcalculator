﻿namespace CostCalculator
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Tree tree = new Tree();
            if (args.Length < 1)
            {
                throw new System.ArgumentException("Filename cannot be empty (Eg. app data.txt)");
            }
            foreach (var level in FileReader.ProcessTreeFromFile(args[0]))
            {
                tree.AddLevel(level);
            }
            System.Console.WriteLine($"Maximum Cost from root to leaf is {tree.GetMaxCost()}");
            System.Console.ReadLine();
        }
    }
}
