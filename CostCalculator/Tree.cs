﻿using System.Linq;

namespace CostCalculator
{
    public class Tree
    {
        private int height = 0;

        public double[] Cost { get; set; } = new double[] { 0 };

        public double GetMaxCost() => this.Cost.Max();

        // Adds a new level using a parsed array of doubles in sequence.
        // The length of the array should be 1 + length of the previous leaf level.
        public void AddLevel(double[] level)
        {
            if (level.Length == height + 1)
            {
                this.UpdateCost(level);
                height++;
            }
            else
            {
                throw new System.ArgumentException($"Number of nodes parsed on this level {level.Length} " +
                    $"does not match required number of nodes {this.height + 1}");
            }
        }

        // Updates cost array using the newLevel as the leaf nodes appended to the bottom of the current tree
        private void UpdateCost(double[] newLevel)
        {
            newLevel[0] += this.Cost[0];
            for (int i = 1; i < newLevel.Length - 1; ++i)
                newLevel[i] += System.Math.Max(this.Cost[i], this.Cost[i - 1]);
            if (newLevel.Length > 1)
                newLevel[newLevel.Length - 1] += this.Cost.Last();
            this.Cost = newLevel;
        }

        // For debugging
        public override string ToString()
        {
            return "Root-to-Leaf Costs for Current Tree: " +
                $"{string.Join(", ", this.Cost.Select(x => x.ToString()).ToArray())}";
        }
    }
}
